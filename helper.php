<?php
function create_database_connection(){

	try {
	    $pdo = new PDO('mysql:host=localhost;dbname=ulhtbook', 'root', '');
	    return $pdo;
	} catch (PDOException $e) {
	    print $e->getMessage();
	    return false;
	}

}

function destroy_database_connection(&$pdo){
	$pdo = null;
}

function get_all_users($pdo){
	$statement = $pdo->prepare("SELECT * FROM user");
	$statement->execute();
	$all_users = $statement->fetchAll();
	return $all_users;
}

function get_a_user($pdo,$id){
	$statement = $pdo->prepare("SELECT * FROM user WHERE id= :id");
	$statement->bindParam(':id', $id, PDO::PARAM_INT);

	$statement->execute();
	$a_user = $statement->fetchAll();
	return $a_user;
}

function edit_a_user($name,$foto,$age,$work,$id,$pdo){
   $statement = $pdo->prepare("UPDATE user SET name=:name,foto=:foto, age=:age, work=:work  WHERE id = :id");
   $statement->bindParam(':name', $name, PDO::PARAM_STR);
   $statement->bindParam(':foto', $foto, PDO::PARAM_STR);
   $statement->bindParam(':age	', $age, PDO::PARAM_STR);
   $statement->bindParam(':work', $work, PDO::PARAM_STR);
   $statement->bindParam(':id', $id, PDO::PARAM_INT);
   $statement->execute();
   //$a_user = $statement->fetchAll();
	//return $a_user;
   //destroy_database_connection($pdo);
}

function apagar_user($id,$pdo){
$statement = $pdo->prepare("DELETE FROM user WHERE id=:id");
   $statement->bindParam(':id', $id, PDO::PARAM_INT);
$statement->execute();
   }

function criar_user($name,$foto,$age,$work,$pdo){
$statement = $pdo->prepare("INSERT INTO user(name, foto, age, work) VALUES ( :name, :foto, :age, :work)");
   $statement->bindParam(':name', $name, PDO::PARAM_STR);
   $statement->bindParam(':foto', $foto, PDO::PARAM_STR);
   $statement->bindParam(':age', $age, PDO::PARAM_INT);
   $statement->bindParam(':work', $work, PDO::PARAM_STR);
$statement->execute();
   }


function draw_user_tablea($all_users){
	?>
	<table border='1'>
	<tr>
	    <th>Identificacao</th>
	    <th>Nome</th> 
	    <th>Foto</th>
	    <th>Idade</th> 
	    <th>Profissão</th>
	    <th>Ver Utilizador</th>
	    <th>Editar</th>
	    <th>Eliminar</th>
  	</tr>
  <?php
	foreach($all_users as $all_users){
		echo "<tr>";
		echo "<td>".$all_users["id"]."</td>";
		echo "<td>".$all_users["name"]."</td>";
		echo "<td><img src='".$all_users["foto"]."'</td>";
		echo "<td>".$all_users["age"]."</td>";
		echo "<td>".$all_users["work"]."</td>";
		echo "<td><a href='/veruser.php?id="   .$all_users["id"]. "'>Ver</a></td>";
		echo "<td><a href='/editaruser.php?id="   .$all_users["id"]. "'>Editar</a></td>";
		echo "<td><a href='/apagaruser.php?id="   .$all_users["id"]. "'>Apagar</a></td>";
		echo "</tr>";
	}
	echo "</table>";
	?>
	<a href="/criaruser.php">Adicionar Um user</a>
	<?php
}

function draw_user_tableg($all_users){
	?>
	<table border='1'>
	<tr>
	    <th>Identificacao</th>
	    <th>Nome</th> 
	    <th>Foto</th>
	    <th>Idade</th> 
	    <th>Profissão</th>
	    <th>Ver Utilizador</th>
  	</tr>
  <?php
	foreach($all_users as $all_users){
		echo "<tr>";
		echo "<td>".$all_users["id"]."</td>";
		echo "<td>".$all_users["name"]."</td>";
		echo "<td><img src='".$all_users["foto"]."'</td>";
		echo "<td>".$all_users["age"]."</td>";
		echo "<td>".$all_users["work"]."</td>";
		echo "<td><a href='/veruser.php?id="   .$all_users["id"]. "'>Ver</a></td>";
		echo "</tr>";
	}
	echo "</table>";
}

function draw_user_tableu($all_users){
	?>
	<table border='1'>
	<tr>
	    <th>Identificacao</th>
	    <th>Nome</th> 
	    <th>Foto</th>
	    <th>Idade</th> 
	    <th>Profissão</th>
	    <th>Ver Utilizador</th>
	    <th>Editar</th>
	    <th>Eliminar</th>
  	</tr>
  <?php
	foreach($all_users as $all_users){
		echo "<tr>";
		echo "<td>".$all_users["id"]."</td>";
		echo "<td>".$all_users["name"]."</td>";
		echo "<td><img src='".$all_users["foto"]."'</td>";
		echo "<td>".$all_users["age"]."</td>";
		echo "<td>".$all_users["work"]."</td>";
		echo "<td><a href='/veruser.php?id="   .$all_users["id"]. "'>Ver</a></td>";
		echo "<td><a href='/editaruser.php?id="   .$all_users["id"]. "'>Editar</a></td>";
		echo "<td><a href='/apagaruser.php?id="   .$all_users["id"]. "'>Apagar</a></td>";
		echo "</tr>";
	}
	echo "</table>";
	?>

	<?php
}

?>