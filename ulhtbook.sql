-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14-Dez-2016 às 18:45
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ulhtbook`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `foto` text NOT NULL,
  `age` int(11) DEFAULT NULL,
  `work` text,
  `url` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `name`, `foto`, `age`, `work`, `url`) VALUES
(1, 'Joao Ratao', '\\images\\cristiano_ronaldo.jpg', 18, 'Pasteleiro', ''),
(7, 'Maria', '\\images\\real_madrid.jpg', NULL, NULL, NULL),
(8, 'Maria', 'images\\real_madrid.jpg', NULL, NULL, NULL),
(18, 'tio', 'rgfwsggrs', NULL, NULL, NULL),
(19, 'Mario', 'feio', NULL, NULL, NULL),
(21, 'Maria Cebola', 'jrsngsool', 14, 'Pastel', NULL);

--
-- Acionadores `user`
--
DELIMITER $$
CREATE TRIGGER `url` AFTER INSERT ON `user` FOR EACH ROW UPDATE user SET url=(SELECT MAX(id) FROM user) WHERE id=(SELECT MAX(id) FROM user)
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
